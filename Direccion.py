# Clase Direccion

class Direccion:
    def __init__(self, pCalle, pProvincia, pCP):
        self.__calle = pCalle
        self.__provincia = pProvincia
        self.__cP = pCP
    
    def setCalle(self, pCalle):
        self.__calle = pCalle
    def getCalle(self):
        return self.__calle
    
    def setProvincia(self, pProvincia):
        self.__provincia = pProvincia
    def getProvincia(self):
        return  self.__provincia
    
    def getCP(self, pCP):
        self.__cP = pCP
    def setCP(self):
        return self.__cP

    def __str__(self):
        return str("\n") + " - Calle: " + str(self.__calle) + str("\n") + " - Provincia: " + str(self.__provincia) + str("\n") + \
         " - Código Postal: " + str(self.__cP)

"""
miDireccion = Direccion("Calle de las Rosas", "Barcelona", 4563540)
print(miDireccion)
"""



