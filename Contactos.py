# Ejercicio Vamos a hacer un sistema para la administración de nuestros contactos

class Contactos:
    def __init__(self, pNombre, pApellidos, pMovil):

        self.__nombre = pNombre
        self.__apellidos = pApellidos
        self.__movil = pMovil


    def setNombre(self, pNombre):
        self.__nombre = pNombre
    def getNombre(self):
        return self.__nombre

    def setApellidos(self, pApellidos):
        self.__apellidos = pApellidos
    def getApellidos(self):
        return self.__apellidos

    def setMovil(self, pMovil):
        self.__movil = pMovil
    def getMovil(self):
        return self.__movil
    
    def __str__(self):
        return " - Nombre: " +  str(self.__nombre) + str("\n") + " - Apellidos: " + str(self.__apellidos) + str("\n") + " - Teléfono Móvil: " \
         + str(self.__movil)


misContactos = Contactos("Andrés", "Nieto Cáceres", 6547852)
print(misContactos)

